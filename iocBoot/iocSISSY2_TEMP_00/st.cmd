#!../../bin/linux-x86_64/SISSY2_TEMP_00

#- You may have to change SISSY2_TEMP_00 to something else
#- everywhere it appears in this file

< envPaths

epicsEnvSet("STREAM_PROTOCOL_PATH", "/opt/epics/ioc/protocols" )
cd "${TOP}"

# Use as a genric docker iocBoot
epicsEnvSet("SYS", "$(IOC_SYS)")
epicsEnvSet("DEV", "$(IOC_DEV)")
epicsEnvSet("EXP", "$(IOC_EXP)")
epicsEnvSet("ENGINEER","Marcel Bajdel")
epicsEnvSet("LOCATION", "OASE")

#epicsEnvSet("ALIVE_SERVER","sissy-serv-04.exp.helmholtz-berlin.de")
 
## Register all support components
dbLoadDatabase "dbd/SISSY2_TEMP_00.dbd"
SISSY2_TEMP_00_registerRecordDeviceDriver pdbbase

# Use the following commands for TCP/IP

#drvAsynIPPortConfigure(const char *portName,
#                       const char *hostInfo,
#                       unsigned int priority,
#                       int noAutoConnect,
#                       int noProcessEos);

drvAsynIPPortConfigure("WG1","192.168.169.75:502",0,0,1)

#drvAsynIPPortConfigure("GW1","192.168.169.115:1026",0,0,1)

#modbusInterposeConfig(const char *portName,
#                      modbusLinkType linkType,
#                      int timeoutMsec, 
#                      int writeDelayMsec)
#configure for rtu
modbusInterposeConfig( "WG1",0,5000,0)


## Holding Registers ##
#######################

#drvModbusAsynConfigure(
#                       Port    TCP  Slave Modbus   Modbus    Modbus Modbus   poll plcType
#                       Name    Port Addr. Function StartAddr length Datatype ms
drvModbusAsynConfigure("rdWord","WG1",0,   3,       0,        32,    4,       1000, "Wago")



set_requestfile_path("$(TOP)/SISSY2_TEMP_00App/Db", "")
set_savefile_path("/opt/epics/autosave/$(IOC_SYS)_$(IOC_EXP)")
set_pass0_restoreFile("settings.sav", "SYS=$(SYS), DEV=$(EXP)")
save_restoreSet_DatedBackupFiles(0)
## Load record instances
#dbLoadRecords("$(ALIVE)/db/alive.db", "P=$(SYS):$(DEV):,RHOST=$(ALIVE_SERVER)" )
#dbLoadTemplate("db/gw.sub", "SYS=$(SYS), DEV=$(DEV1),EXP=$(EXP)")
#dbLoadRecords("db/pid.db", "SYS=$(SYS), EXP=$(EXP)")
dbLoadRecords("db/temp.db", "SYS=$(SYS), DEV=$(EXP):01,MODBUS_PORT=rdWord, CHANNEL=0")
dbLoadRecords("db/temp.db", "SYS=$(SYS), DEV=$(EXP):02,MODBUS_PORT=rdWord, CHANNEL=1")
dbLoadRecords("db/temp.db", "SYS=$(SYS), DEV=$(EXP):03,MODBUS_PORT=rdWord, CHANNEL=2")
dbLoadRecords("db/temp.db", "SYS=$(SYS), DEV=$(EXP):04,MODBUS_PORT=rdWord, CHANNEL=3")
dbLoadRecords("db/temp.db", "SYS=$(SYS), DEV=$(EXP):05,MODBUS_PORT=rdWord, CHANNEL=4")
dbLoadRecords("db/temp.db", "SYS=$(SYS), DEV=$(EXP):06,MODBUS_PORT=rdWord, CHANNEL=5")
dbLoadRecords("db/temp.db", "SYS=$(SYS), DEV=$(EXP):07,MODBUS_PORT=rdWord, CHANNEL=6")
dbLoadRecords("db/temp.db", "SYS=$(SYS), DEV=$(EXP):08,MODBUS_PORT=rdWord, CHANNEL=7")
#var streamDebug 1



cd "${TOP}/iocBoot/${IOC}"
iocInit

dbl > /opt/epics/ioc/log/oase.dbl
